package com.watsonyao.android.dagger;

/**
 * Created by watson on 17/6/13.
 */

public interface MainView {

    void showMessage(String msg);
}
