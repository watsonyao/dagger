package com.watsonyao.android.dagger;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

/**
 * Created by watson on 17/6/13.
 */
@Module(subcomponents = {
        MainActivityComponent.class,
})
public abstract class MainBuildersModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity>
    bindMainActivityFactory(MainActivityComponent.Builder builder);
}
