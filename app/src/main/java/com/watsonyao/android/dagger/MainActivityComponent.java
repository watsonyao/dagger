package com.watsonyao.android.dagger;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by watson on 17/6/13.
 */

@Subcomponent(
        modules = MainModule.class
)
public interface MainActivityComponent extends AndroidInjector<MainActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity> {

    }
}
