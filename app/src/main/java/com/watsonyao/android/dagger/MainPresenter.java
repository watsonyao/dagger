package com.watsonyao.android.dagger;

import javax.inject.Inject;

/**
 * Created by watson on 17/6/13.
 */

public class MainPresenter {

    private MainView mainView;

    @Inject
    MainPresenter(MainView mainView) {
        this.mainView = mainView;
    }

    void start() {
        mainView.showMessage("message");
    }
}
