package com.watsonyao.android.dagger;

import dagger.Binds;
import dagger.Module;

/**
 * Created by watson on 17/6/13.
 */
@Module
abstract class MainModule {

    @Binds
    abstract MainView bindMainView(MainActivity activity);
}
