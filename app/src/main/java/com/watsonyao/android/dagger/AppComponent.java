package com.watsonyao.android.dagger;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by watson on 17/6/13.
 */

@Component(
        modules = {
                AndroidInjectionModule.class,
                AndroidSupportInjectionModule.class,
                MainBuildersModule.class
        }
)
public interface AppComponent {

    MainApplication inject(MainApplication appContext);
}
